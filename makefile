OBJ = huffman.o
FLAGS = -Wall -std=c99 -g

all:
	make compress
	make decompress

compress: compress.c $(OBJ)
	gcc compress.c $(OBJ) -o compress $(FLAGS)

decompress: decompress.c $(OBJ)
	gcc decompress.c $(OBJ) -o decompress $(FLAGS)

huffman.o: huffman.c huffman.h
	gcc huffman.c -c $(FLAGS)

test: test.c huffman.c huffman.h
	gcc test.c -o test $(FLAGS)

clean:
	rm *.o compress decompress test -f
