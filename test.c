#include <stdio.h>
#include <stdlib.h>

#include "huffman.c"

#define LEN(v) sizeof (v) / sizeof (v[0])

void repr_print(Repr * r)
{
    char bits[32];
    int len = 0, n = r->n;

    for (uint16_t mask = 0x8000; n != 0; n--, mask >>= 1)
        bits[len++] = r->repr & mask ? '1' : '0';

    bits[len++] = 0;

    printf("%c: %s\n", r->c, bits);
}

void struct_test()
{
    Node v[] = {
        {'a', 7},
        {' ', 4},
        {'o', 3},
        {'r', 3},
        {'c', 2},
        {'n', 2},
        {'d', 1},
        {'e', 1},
        {'g', 1},
        {'i', 1},
        {'m', 1},
        {'s', 1},
        {'t', 1},
    };

    Heap freq = { malloc(256 * sizeof (Node*)), 0};
    ReprArray repr = { malloc(256 * sizeof (Repr)), 0};

    for (int i = 0; i < LEN(v); i++)
        heap_insert(&freq, v+i);

    huf_build_tree(&freq);
    huf_build_repr(&repr, freq.v[0], 0);

    for (int i = 0; i < repr.size; i++)
        repr_print(repr.v + i);


    // Test heap
    Heap h2 = { malloc(256 * sizeof (Node*)), 0};

    for (int i = 0; i < LEN(v); i++)
        heap_insert(&h2, v+i);

    while (h2.size > 0)
    {
        printf("top {%c: %d}\n", h2.v[0]->c, h2.v[0]->freq);
        heap_remove(&h2, 0);
    }
}

void bitlength_test()
{
    Node v[256];

    for (int i = 0; i < 256; i++)
        v[i] = (Node) {(char) i, i};

    Heap * freq = new_heap(256);
    ReprArray * array = new_reprarray(257);

    for (int i = 0; i < 256; i++)
        heap_insert(freq, v+i);

    huf_build_tree(freq);
    huf_build_repr(array, freq->v[0], 0);

    for (int i = 0; i < array->size; i++)
        repr_print(array->v + i);
}

void bitstream_test()
{
    const static char str[] = "01001100011100001111";
    FILE * fp;
    BitStream * bs;

    printf("bits: %lu\n", sizeof (str) - 1);

    printf("before %s\n", str);

    fp = fopen("bs.bin", "w+");
    bs = new_bitstream(fp, true);

    for (int i = 0; i < sizeof (str) - 1; i++)
        bs_put(bs, str[i] - '0');

    bs_write_flush(bs);
    fclose(fp);
    free(bs);

    printf("after  ");

    fp = fopen("bs.bin", "r");
    bs = new_bitstream(fp, false);

    for (int i = 0; i < sizeof (str) - 1; i++)
        printf("%c", bs_next(bs) + '0');

    fclose(fp);
    free(bs);

    printf("\n");
}

int main()
{
    // bitlength_test();
    bitstream_test();

    int len = huf_compress("example.huf", "example.txt");
    if (len >= 0)
        fprintf(stdout, "%d %s\n", len, len > 1 ? "bytes escritos." : "byte escrito");
    else
        perror("Falha ao comprimir arquivo example.txt");

    len = huf_decompress("example.out.txt", "example.huf");
    if (len >= 0)
        fprintf(stdout, "%d %s\n", len, len > 1 ? "bytes escritos." : "byte escrito");
    else
        perror("Falha ao descomprimir arquivo example.huf");

    return 0;
}
