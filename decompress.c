#include <stdio.h>
#include <string.h>

#include "huffman.h"

int get_file_size(const char * path)
{
    FILE * fp = fopen(path, "r");
    int size;

    if (!fp)
        return -1;

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);

    fclose(fp);
    return size;
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s <file.txt.bin>\n"
            "\tfile.txt.bin: the file to be decompressed into file.txt\n", argv[0]);
        return 0;
    }

    char output[256];
    int len = strlen(argv[1]);

    if (len <= 4 || strcmp(argv[1]+(len-4), ".bin") != 0)
    {
        fprintf(stderr, "Please, give me .bin file\n");
        return -1;
    }

    strncpy(output, argv[1], len);

    int val = huf_decompress(output, argv[1]);

    if (val < 0)
    {
        perror("Falha ao descomprimir o arquivo");
        return -1;
    }

    return 0;
}
