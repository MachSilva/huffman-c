#include <stdio.h>
#include <string.h>

#include "huffman.h"

int get_file_size(const char * path)
{
    FILE * fp = fopen(path, "r");
    int size;

    if (!fp)
        return -1;

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);

    fclose(fp);
    return size;
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s <file.txt>\n"
            "\tfile.txt: the file to be compressed into file.bin\n", argv[0]);
        return 0;
    }

    char output[256];
    strcpy(output, argv[1]);
    strcat(output, ".bin");

    int val = huf_compress(output, argv[1]);

    if (val < 0)
    {
        perror("Falha ao comprimir o arquivo");
        return -1;
    }

    int orig = get_file_size(argv[1]);
    int outp = get_file_size(output);
    float ratio = 100.0 * outp / orig;

    printf(
        "Arquivo original:   %8d bytes\n"
        "Arquivo comprimido: %8d bytes\n"
        "Taxa de compressão: %3.2f%%\n", orig, outp, ratio);

    return 0;
}
