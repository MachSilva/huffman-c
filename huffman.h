#pragma once

#include <stdio.h>

int huf_compress(const char * dest, const char * src);
int huf_decompress(const char * dest, const char * src);
