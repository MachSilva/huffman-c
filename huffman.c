#include "huffman.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
    uint8_t c;
    uint16_t freq;
} CharCounter;

/* 4-byte, one double word structure */
typedef struct {
    uint8_t c;
    uint16_t repr;
    uint8_t n;
} Repr;

typedef struct {
    Repr * v;
    int size;
} ReprArray;

typedef struct Node__ {
    uint8_t c;
    uint16_t freq;
    struct Node__ * left;
    struct Node__ * right;
} Node;

typedef struct {
    Node ** v;
    int size;
} Heap;

typedef struct {
    FILE * fp;
    uint8_t buf, mask;
} BitStream;

// bit stream
static int bs_next(BitStream * bs)
{
    int result = (bs->buf & bs->mask) ? 1 : 0;
    bs->mask >>= 1;

    if (!bs->mask)
    {
        bs->mask = 0x80;
        fread(&bs->buf, sizeof (bs->buf), 1, bs->fp);
    }

    return result;
}

static void bs_put(BitStream * bs, int bit)
{
    if (bit)
        bs->buf |= bs->mask;
    bs->mask >>= 1;

    if (!bs->mask)
    {
        fwrite(&bs->buf, sizeof (bs->buf), 1, bs->fp);
        bs->mask = 0x80;
        bs->buf = 0;
    }
}

static void bs_write_flush(BitStream * bs)
{
    if (bs->mask != 0x80)
    {
        fwrite(&bs->buf, sizeof (bs->buf), 1, bs->fp);
        bs->buf = 0;
        bs->mask = 0x80;
    }
}

static BitStream * new_bitstream(FILE * fp, int write_flag)
{
    BitStream * ptr = malloc(sizeof (BitStream));
    *ptr = (BitStream) {fp, 0, 0x80};

    if (!write_flag)
    {
        *ptr = (BitStream) {fp, 0, 0x80};
        fread(&ptr->buf, sizeof (ptr->buf), 1, fp);
    }

    return ptr;
}

// Min Heap / Priority Queue

static int node_compar(const Node * a, const Node * b)
{
    if (a->freq == b->freq)
        return a->c - b->c;
    return a->freq - b->freq;
}

static void node_ptr_swap(Node ** a, Node ** b)
{
    Node * s = *a;
    *a = *b;
    *b = s;
}

static inline bool node_is_leaf(Node * p)
{
    return !p->left && !p->right;
}

static inline int heap_Lchild(int i)
{
    return 2*i + 1;
}

static inline int heap_Rchild(int i)
{
    return 2*i + 2;
}

static inline int heap_parent(int i)
{
    return (i - 1) / 2;
}

// XXX refactor: from recursive to linear!
static void heap_up(Heap * h, int i)
{
    if (i <= 0)
        return;

    int p = heap_parent(i);
    if (node_compar(h->v[p], h->v[i]) > 0) // Min Heap; lesser nodes up
    {
        node_ptr_swap(h->v+p, h->v+i);
        heap_up(h, p);
    }
}

static void heap_insert(Heap * h, Node * k)
{
    h->v[h->size] = k;
    heap_up(h, h->size++);
}

// XXX refactor: remove recursion!
static void heap_down(Heap * h, int i)
{
    if (i >= h->size)
        return;

    int j, L = heap_Lchild(i), R = heap_Rchild(i);

    if (L >= h->size)
        return;
    if (R >= h->size)
        j = L;
    else
        j = node_compar(h->v[L], h->v[R]) < 0 ? L : R;

    if (node_compar(h->v[i], h->v[j]) > 0)
    {
        node_ptr_swap(h->v + i, h->v + j);
        heap_down(h, j);
    }
}

static void heap_remove(Heap * h, int i)
{
    if (h->size < 1 || i < 0)
        return;

    int sz = --h->size;
    node_ptr_swap(h->v+i, h->v+sz);
    heap_down(h, i);
}

static Heap * new_heap(int max_size)
{
    Heap * ptr = malloc(sizeof (Heap));
    *ptr = (Heap) { malloc(max_size * sizeof (Node*)), 0 };
    return ptr;
}

static void delete_heap(Heap * ptr)
{
    // TODO Delete all nodes! Avoid Memory Leak!
    free(ptr->v);
    free(ptr);
}

static ReprArray * new_reprarray(int max_size)
{
    ReprArray * ptr = malloc(sizeof (ReprArray));
    *ptr = (ReprArray) { malloc((max_size) * sizeof (Repr)), 0 };
    return ptr;
}

static void delete_reprarray(ReprArray * ptr)
{
    free(ptr->v);
    free(ptr);
}

/* Transforma as frequências de caracteres na heap e cria a árvore
 * de Huffman.
 * É extremamente necessário que a heap tenha pelo menos o dobro do próprio
 * tamanho alocado para evitar falhas de segmentação.
 */
static void huf_build_tree(Heap * frequencies)
{
    while (frequencies->size > 1)
    {
        Node * one, * two;

        one = frequencies->v[0];
        heap_remove(frequencies, 0);
        two = frequencies->v[0];
        heap_remove(frequencies, 0);

        Node * merge = malloc(sizeof (Node));
        *merge = (Node) {one->c, one->freq + two->freq, one, two};

        heap_insert(frequencies, merge);
    }
}

/* bit  from left to right */
static void repr_bit_set(Repr * r, uint8_t bit, uint8_t value)
{
    // It's bit operation! Caution here
    if (value)  // Set bit
        r->repr |= 0x8000 >> bit;
    else        // Unset bit
        r->repr &= ~(0x8000 >> bit);
}

static void huf_build_repr(ReprArray * a, Node * n, uint8_t depth)
{
    if (!n)
        return;

    // Nó folha
    if (node_is_leaf(n))
    {
        int i = a->size++;
        a->v[i].c = n->c;
        a->v[i].n = depth;
        // XXX this could be improved; it requires one more position in array
        a->v[i+1] = a->v[i];
    }
    else
    {
        repr_bit_set(a->v + a->size, depth, 0);
        huf_build_repr(a, n->left, depth + 1);
        repr_bit_set(a->v + a->size, depth, 1);
        huf_build_repr(a, n->right, depth + 1);
    }
}

#define CHARS 256
int huf_compress(const char * dest, const char * src)
{
    int length;
    // Reserve resources, open files
    FILE * p_dest = fopen(dest, "w+");
    FILE * p_src = fopen(src, "r");

    // If failed to open any file, return
    if (!p_dest || !p_src)
        return -1;

    Heap * freq = new_heap(CHARS);
    ReprArray * map = new_reprarray(CHARS+1);
    // Faster access to representations in map
    Repr ** indexmap = calloc((CHARS+1), sizeof (Repr*));

    // Count chars
    CharCounter counter[CHARS];
    uint8_t c_len = 0;
    int c;

    // Initialize counter
    for (int i = 0; i < CHARS; i++)
        counter[i] = (CharCounter) {i, 0};

    // Read char per char (or byte per byte)
    while ((c = fgetc(p_src)) != EOF)
        counter[c].freq++;

    // Remove chars with no occurrence
    for (int i = 0; i < CHARS; i++)
    {
        if (counter[i].freq != 0)
        {
            counter[c_len] = counter[i];
            counter[i].freq = 0;
            c_len++;
        }
    }

    // Build heap
    for (int i = 0; i < c_len; i++)
    {
        Node * p = malloc(sizeof (Node));
        *p = (Node) {counter[i].c, counter[i].freq, 0};
        heap_insert(freq, p);
    }

    // Build tree, then build a representation that maps char to bit sequence
    huf_build_tree(freq);
    huf_build_repr(map, freq->v[0], 0);

    // Mapping into indexmap
    for (int i = 0; i < map->size; i++)
    {
        uint8_t ch = map->v[i].c;
        indexmap[ch] = map->v+i;
    }

    // Write table length
    fwrite(&c_len, sizeof (uint8_t), 1, p_dest);
    // Write table
    fwrite(counter, sizeof (CharCounter), c_len, p_dest);
    // Write total chars from source
    length = ftell(p_src);
    fwrite(&length, sizeof (length), 1, p_dest);

    if (ferror(p_dest))
        return -1;

    // Finally write compressed characters. Prepare for read from source
    fseek(p_src, 0, SEEK_SET);

    if (ferror(p_src))
        return -1;

    // Write!
    // Bit operation... Caution here!
    BitStream * bs = new_bitstream(p_dest, true);
    while ((c = fgetc(p_src)) != EOF)
    {
        uint16_t piece = indexmap[c]->repr;
        uint16_t mask = 0x8000;
        int n = indexmap[c]->n;

        for (int i = 0; mask != 0 && i < n; i++, mask >>= 1)
            bs_put(bs, piece & mask);
    }

    bs_write_flush(bs);

    // Return p_dest file length
    length = ftell(p_dest);

    // Free resources
    fclose(p_src);
    fclose(p_dest);
    delete_heap(freq);
    delete_reprarray(map);
    free(indexmap);
    free(bs);

    return length;
}

int huf_decompress(const char * dest, const char * src)
{
    FILE * p_dest = fopen(dest, "w+");
    FILE * p_src = fopen(src, "r");

    // If failed to open any file, return
    if (!p_dest || !p_src)
        return -1;

    Heap * freq = new_heap(CHARS);

    uint8_t c_len;
    fread(&c_len, sizeof (c_len), 1, p_src);

    if (ferror(p_src))
        return -1;

    // Read table
    CharCounter * table = malloc(c_len * sizeof (CharCounter));
    fread(table, sizeof (CharCounter), c_len, p_src);

    // Build tree
    for (int i = 0; i < c_len; i++)
    {
        Node * p = malloc(sizeof (Node));
        *p = (Node) { table[i].c, table[i].freq };
        heap_insert(freq, p);
    }

    huf_build_tree(freq);

    // Read total char count
    int length;
    fread(&length, sizeof (length), 1, p_src);

    // Match bits!
    BitStream * bs = new_bitstream(p_src, false);

    int n = length;
    while (n-- > 0)
    {
        Node * p = freq->v[0];

        while (!node_is_leaf(p))
            p = bs_next(bs) ? p->right : p->left;

        fwrite(&p->c, sizeof (p->c), 1, p_dest);
    }

    // Free resources
    fclose(p_src);
    fclose(p_dest);
    delete_heap(freq);
    free(table);
    free(bs);

    return length;
}
#undef CHARS
