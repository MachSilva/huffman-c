# Compressão de Huffman em C

Todo o código de compressão está nos arquivos ```huffman.c``` e ```huffman.h```.
As funções principais são:

* ```int huf_compress(const char * dest, const char * src)```
* ```int huf_decompress(const char * dest, const char * src)```

Ambos retornam inteiros negativos em caso de falha e recebem ```dest``` e ```src``` como parâmetros:

* ```dest``` caminho do arquivo de destino;
* ```src``` caminho do arquivo a ser processado;

## Formato do arquivo comprimido

### Cabeçalho
* **Offset 0** : *(1 byte)* tamanho da *char table*;
* **Offset 1** : *(n bytes)* início da *char table* que contém todos os caracteres utilizados junto com sua frequência contabilizada (e.g. {'c', 3});
* **Offset n + 1** : *(4 bytes)* quantidade de caracteres;

### Conteúdo
* **Offset n + 5** : *(...)* arquivo original codificado
